# BrowserSpy

[BrowserSpy](https://browserspy.io) is a hosted manual testing tool that enables 
you to record exactly what caused a problem. This makes for happier developers,
testers and customers as you no longer have to waste time on working out what
happened - you can just watch it.

This repository contains the [central issue tracker](https://gitlab.com/browserspy/browserspy/issues)
for the BrowserSpy project.

## Documentation

Documentation for the BrowserSpy project can be found at
<https://docs.browserspy.io>.
